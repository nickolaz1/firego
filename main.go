package main

import (
	"github.com/gorilla/mux"
	"net/http"
	"time"
	"encoding/json"
	"os"
	"gopkg.in/zabawaba99/firego.v1"
	"log"
	"fmt"
)

type Foco struct {
	Accion	string    `json:"accion"`
	Fecha string    `json:"fecha"`
	Descripcion string	`json:"tipo_actuador"`
}

type User struct {
	Mail	string    `json:"mail"`
	Pass	string    `json:"pass"`
	User	string    `json:"user"`
}

type Historial struct {
	User 		string `json:"User"`
	Accion		string `json:"accion"`
	Fecha		string `json:"fecha"`
	Descripcion	string `json:"tipo_actuador"`
}

type Pi struct {
	cpuTemp		string `json:"cpu_temp"`
	diskFree	string `json:"disk_free"`
	diskPerc	string `json:"disk_perc"`
	diskTotal	string `json:"disk_total"`
	ramFree		string `json:"ram_free"`
	ramTotal	string `json:"ram_total"`
	ramUsed		string `json:"ram_used"`
}

var bd *firego.Firebase

func main() {
	bd = firego.New("https://domobd-de72d.firebaseio.com", nil)
	token := "PodqGywVMR0FNWZNRN1ZcQccXUd74H8XcX1RDGSe"
	bd.Auth(token)
	r := mux.NewRouter().StrictSlash(false)
	r.HandleFunc("/actuadores",use(GetFocosHandle,basicAuth)).Methods("GET")
	r.HandleFunc("/usuarios",use(GetUserHandle,basicAuth)).Methods("GET")
	r.HandleFunc("/actuadores/",use(UpdateFocosHandle,basicAuth)).Methods("GET")
	r.HandleFunc("/actuadores/all",use(GetAllFocosHandle,basicAuth)).Methods("GET")
	r.HandleFunc("/pi/",use(UpdatePIHandle,basicAuth)).Methods("GET")
	r.HandleFunc("/pi",use(GetPIHandle,basicAuth)).Methods("GET")
	server := &http.Server{
		Addr:	":"+os.Getenv("PORT"),
		Handler:	r,
		ReadHeaderTimeout:	10*time.Second,
		WriteTimeout:	10*time.Second,
		MaxHeaderBytes:	1<<20,
	}
	println("Listening....")
	server.ListenAndServe()
}

func GetFocosHandle(w http.ResponseWriter, r *http.Request) {
	Act := bd.Child("Actuadores")
	var focos[] Foco
	if err := Act.Value(&focos); err != nil {
		log.Fatal(err)
	}
	w.Header().Set("Content-Type","application/json")
	j,_ := json.Marshal(focos)
	w.WriteHeader(http.StatusOK)
	w.Write(j)
}

func GetUserHandle(w http.ResponseWriter, r *http.Request) {
	Usr := bd.Child("Usuarios")
	var users[] User
	if err := Usr.Value(&users); err != nil {
		log.Fatal(err)
	}
	w.Header().Set("Content-Type","application/json")
	j,_ := json.Marshal(users)
	w.WriteHeader(http.StatusOK)
	w.Write(j)
}

func UpdateFocosHandle(w http.ResponseWriter, r *http.Request) {
	tipo_actuador := r.URL.Query().Get("tipo_actuador")
	accion := r.URL.Query().Get("accion")
	usuario := r.URL.Query().Get("usuario")
	Act := bd.Child("Actuadores")
	casa := "0"
	DirHist := "Historial/"+casa
	His := bd.Child(DirHist)
	var up Historial
	var hist[] Historial
	var focos[] Foco
	if err := Act.Value(&focos); err != nil {
		log.Fatal(err)
	}
	for i := 0 ; i < len(focos) ; i++ {
		if  focos[i].Descripcion == tipo_actuador {
			focos[i].Accion = accion
			focos[i].Fecha = Time().String()
			//cargar historial
			up.User = usuario
			up.Descripcion = focos[i].Descripcion
			up.Accion = focos[i].Accion
			up.Fecha = focos[i].Fecha
		}
	}
	if err := Act.Set(&focos); err != nil {
		log.Fatal(err) //update actuadores
	}
	if err := His.Value(&hist); err != nil {
		log.Fatal(err)
	}
	 hist = append(hist, up)
	if  err := His.Set(&hist); err != nil {
		log.Fatal(err) //update historial
	}
	w.Header().Set("Content-Type","application/json")
	j,_ := json.Marshal(focos)
	w.WriteHeader(http.StatusCreated)
	w.Write(j)
}

func GetAllFocosHandle(w http.ResponseWriter, r *http.Request) {
	casa := "0"
	DirHist := "Historial/"+casa
	His := bd.Child(DirHist)
	var hist[] Historial
	if err := His.Value(&hist); err != nil {
		log.Fatal(err)
	}
	w.Header().Set("Content-Type","application/json")
	j,_ := json.Marshal(hist)
	w.WriteHeader(http.StatusOK)
	w.Write(j)
}

func Time() time.Time{
	h := int64(14400000) // 4 horas en milisegundos
	t := time.Now()
	m :=(t.Round(time.Millisecond).UnixNano()- h ) / (int64(time.Millisecond) / int64(time.Nanosecond))
	return time.Unix(m/1e3, (m%1e3)*int64(time.Millisecond)/int64(time.Nanosecond))
}

func use(h http.HandlerFunc, middleware ...func(http.HandlerFunc) http.HandlerFunc) http.HandlerFunc {
	for _, m := range middleware {
		h = m(h)
	}
	return h
}

func basicAuth(h http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("WWW-Authenticate", `Basic realm="User Visible Realm"`)
		usr, pass, _ := r.BasicAuth() //extrae los usuarios de la peticion
		fmt.Println(usr)
		fmt.Println(pass)
		auth := false
		Usr := bd.Child("Usuarios")
		var users[] User
		if err := Usr.Value(&users); err != nil {
			log.Fatal(err)
		}
		for _, u := range users{
			if u.User == usr && u.Pass == pass { auth = true}
		}
		if(auth == false){
			http.Error(w, "Not authorized", 401)
			return
		}
		h.ServeHTTP(w, r)
	}
}

func GetPIHandle(w http.ResponseWriter, r *http.Request) {
	Act := bd.Child("maq/0")
	//var p Pi
	var v map[string]interface{}
	if err := Act.Value(&v); err != nil {
		log.Fatal(err)
	}
	log.Println(v)
	w.Header().Set("Content-Type","application/json")
	j,_ := json.Marshal(v)
	w.WriteHeader(http.StatusOK)
	w.Write(j)
}

func UpdatePIHandle(w http.ResponseWriter, r *http.Request) {
	cpuTemp := r.URL.Query().Get("cpu_temp")
	ramTotal := r.URL.Query().Get("ram_total")
	ramUsed := r.URL.Query().Get("ram_used")
	ramFree := r.URL.Query().Get("ram_free")
	diskTotal := r.URL.Query().Get("disk_total")
	diskFree := r.URL.Query().Get("disk_free")
	diskPerc := r.URL.Query().Get("disk_perc")
	Act := bd.Child("maq/0")
	v1 := map[string]string{"cpu_temp":cpuTemp,"disk_free":diskFree,
							"disk_perc":diskPerc,"disk_total":diskTotal,
							"ram_free":ramFree,"ram_total":ramTotal,"ram_used":ramUsed}
	var v map[string]interface{}
	if err := Act.Set(&v1); err != nil {
		log.Fatal(err) //update bd
	}
	if err := Act.Value(&v); err != nil {
		log.Fatal(err)
	}
	w.Header().Set("Content-Type","application/json")
	j,_ := json.Marshal(v)
	w.WriteHeader(http.StatusCreated)
	w.Write(j)
}